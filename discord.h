#ifndef CDISCORD_DISCORD_H
#define CDISCORD_DISCORD_H

#include "cjson/cJSON.h"
#include "string.h"

size_t appendChunk(char* ptr, size_t size, size_t nmemb, string* s);
string APIRequestGET(string URI, int verbose);
string APIRequestPOST(string URI, string data, int verbose);
cJSON* getGuilds();
cJSON* getChannels(char* guildId);
cJSON* getChannel(char* channelId);
cJSON* getMessages(char* channelId);
cJSON* sendMessage(char* channelId, char* message);

#endif