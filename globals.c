//
// Created by PM on 13/06/2021.
//

#include <curses.h>
#include <curl/curl.h>
#include <locale.h>
#include "cjson/cJSON.h"
#include "globals.h"
#include "util.h"

string userToken;
int userIsBot;

void globals_init() {
  curl_global_init(CURL_GLOBAL_ALL);
  initscr();
  curs_set(0);
  keypad(stdscr, TRUE);
  noecho();
  setlocale(LC_ALL, "");

  string configString = readFile("config.json");
  cJSON* configJSON = cJSON_ParseWithLength(configString.ptr, configString.length);

  string_init(&userToken);

  string_append(&userToken, cJSON_GetObjectItem(configJSON, "token")->valuestring);
  userIsBot = cJSON_IsTrue(cJSON_GetObjectItem(configJSON, "isBot"));

  string_destroy(&configString);
  cJSON_Delete(configJSON);
}

void globals_destroy() {
  string_destroy(&userToken);
  endwin();
  curl_global_cleanup();
}

char* getUserToken() {
  return userToken.ptr;
}

int getUserIsBot() {
  return userIsBot;
}