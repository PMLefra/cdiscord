# CDiscord
A fast & easy-to-use Discord client inside a terminal emulator, written in C.  

## Dependencies:
- cmake (to build the project), along with a correct toolchain
- curl (libcurl)
- ncurses (libncurses)

## Setup
Assuming you are in the main project directory, build with:
```sh
mkdir build
cmake -S . -B build
cmake --build build --target cdiscord
```
Execute with `./cdiscord`.

### Required `config.json` file:
```
{
  "token": "Your token",
  "isBot": <true or false, depending on your account type>
}
```
