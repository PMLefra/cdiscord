#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include "cjson/cJSON.h"
#include "discord.h"
#include "globals.h"
#include "util.h"

int main(int argc, char** argv) {
  if (argc > 1 && (strcmp(argv[1], "-v") == 0 || strcmp(argv[1], "--version") == 0)) {
    printf("CDiscord version %s\n", CDiscord_VERSION);
    return 0;
  }

  globals_init();

  if (has_colors() == FALSE) {
    endwin();
    printf("Your terminal does not support color\n");
    exit(1);
  }
  start_color();
  init_pair(1, COLOR_BLACK + COLOR_INTENSE, COLOR_BLACK);
  init_pair(2, COLOR_BLUE + COLOR_INTENSE, COLOR_BLACK);
  init_pair(3, COLOR_YELLOW, COLOR_BLACK);

  printProgramHeader();
  int entryPointHeight = getcury(stdscr);

  cJSON* guilds = getGuilds();
  cJSON* currentGuild = guilds->child;
  stringList guildIdList = stringList_empty();
  stringList guildNameList = stringList_empty();
  int guildIndex = 0;

  while (currentGuild != NULL) {
    char* id = cJSON_GetObjectItem(currentGuild, "id")->valuestring;
    char* name = cJSON_GetObjectItem(currentGuild, "name")->valuestring;

    string idString = string_empty();
    string_append(&idString, id);
    stringList_append(guildIdList, idString);
    string_destroy(&idString);
    string nameString = string_empty();
    string_append(&nameString, name);
    stringList_append(guildNameList, nameString);
    string_destroy(&nameString);

    guildIndex++;
    currentGuild = currentGuild->next;
  }

  int guildChoice = menu(guildNameList, "---- Select the guild ----", entryPointHeight);

  cJSON* channels = getChannels(stringList_at(guildIdList, guildChoice).ptr);
  cJSON* currentChannel = channels->child;
  stringList channelIdList = stringList_empty();
  stringList channelNameList = stringList_empty();
  int channelIndex = 0;

  while (currentChannel != NULL) {
    int type = cJSON_GetObjectItem(currentChannel, "type")->valueint;
    if (type == 2 || type == 4 || type == 13) {
      currentChannel = currentChannel->next;
      continue;
    }
    char* id = cJSON_GetObjectItem(currentChannel, "id")->valuestring;
    char* name = cJSON_GetObjectItem(currentChannel, "name")->valuestring;

    string idString = string_empty();
    string_append(&idString, id);
    stringList_append(channelIdList, idString);
    string_destroy(&idString);
    string nameString = string_empty();
    string_append(&nameString, "#");
    string_append(&nameString, name);
    stringList_append(channelNameList, nameString);
    string_destroy(&nameString);

    channelIndex++;
    currentChannel = currentChannel->next;
  }

  int channelChoice = menu(channelNameList, "---- Select the channel ----", entryPointHeight);

  move(entryPointHeight, 0);
  clrtoeol();
  printw("Write the message you wish to send:\n");
  refresh();
  string messageToSend = askForString();

  cJSON* messageSentJSON = sendMessage(stringList_at(channelIdList, channelChoice).ptr, messageToSend.ptr);

  attron(COLOR_PAIR(3));
  printw("\nMessage successfully sent!\n");
  printw("Press any key to quit.\n");
  attroff(COLOR_PAIR(3));
  refresh();

  getch();

  stringList_destroy(guildIdList);
  stringList_destroy(guildNameList);
  stringList_destroy(channelIdList);
  stringList_destroy(channelNameList);
  string_destroy(&messageToSend);
  cJSON_Delete(guilds);
  cJSON_Delete(channels);
  cJSON_Delete(messageSentJSON);
  globals_destroy();

  return 0;
}