#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "string.h"

void string_init(string* s) {
  s->length = 0;
  s->ptr = malloc(s->length + 1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc error\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

void string_destroy(string* s) {
  s->length = 0;
  free(s->ptr);
}

string string_empty() {
  string s;
  string_init(&s);
  return s;
}

void string_setLength(string* s, size_t newLength) {
  size_t oldLength = s->length;

  if (oldLength == newLength) return;
  
  s->length = newLength;
  s->ptr = realloc(s->ptr, newLength + 1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc error\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[newLength] = '\0';

  if (oldLength < newLength)
    s->ptr[oldLength] = ' ';
}

void string_appendWithLength(string* s, char* ptr, size_t length) {
  size_t oldLength = s->length;
  string_setLength(s, oldLength + length);
  
  memcpy(s->ptr + oldLength, ptr, length);
}

void string_append(string* s, char* ptr) {
  size_t length = strlen(ptr);
  string_appendWithLength(s, ptr, length);
}

string string_copy(string s) {
  string fstr = string_empty();
  string_append(&fstr, s.ptr);
  return fstr;
}

string string_concat(string s1, string s2) {
  string s;
  
  string_init(&s);
  string_appendWithLength(&s, s1.ptr, s1.length);
  string_appendWithLength(&s, s2.ptr, s2.length);
  
  return s;
}

void string_print(string s) {
  printf("%s\n", s.ptr);
}