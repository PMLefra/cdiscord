//
// Created by PMLefra on 14/06/2021.
//

#ifndef CDISCORD_LIST_H
#define CDISCORD_LIST_H

#include <stddef.h>
#include "string.h"

struct stringList {
  string* val;
  struct stringList* prev;
  struct stringList* next;
};

typedef struct stringList* stringList;

stringList stringList_first(stringList sl);
stringList stringList_last(stringList sl);
void stringList_destroy(stringList sl);
stringList stringList_empty();
void stringList_append(stringList sl, string str);
void stringList_pop(stringList sl);
size_t stringList_length(stringList sl);
string stringList_at(stringList sl, int index);
void stringList_print(stringList sl);

#endif