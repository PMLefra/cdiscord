//
// Created by PMLefra on 13/06/2021.
//

#ifndef CDISCORD_GLOBALS_H
#define CDISCORD_GLOBALS_H
#define CDiscord_VERSION "0.0.2"

void globals_init();
void globals_destroy();
char* getUserToken();
int getUserIsBot();

#endif
