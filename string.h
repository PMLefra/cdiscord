#ifndef CDISCORD_STRING_H
#define CDISCORD_STRING_H

#include <stddef.h>

typedef struct string {
  char* ptr;
  size_t length;
} string;

void string_init(string* s);
void string_destroy(string* s);
string string_empty();
void string_setLength(string* s, size_t newLength);
void string_appendWithLength(string* s, char* ptr, size_t length);
void string_append(string*s, char* ptr);
string string_copy(string s);
string string_concat(string s1, string s2);
void string_print(string s);

#endif
