#ifndef CDISCORD_UTIL_H
#define CDISCORD_UTIL_H
#define COLOR_INTENSE 8

#include <stddef.h>
#include <curses.h>
#include "string.h"
#include "list.h"

string readFile(char* fileName);
int askForInteger();
string askForString();
void printProgramHeader();
int menu(stringList options, char* title, int height);

#endif
