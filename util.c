#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <menu.h>
#include "util.h"

#define MAX_INPUT_LENGTH 2000

string readFile(char* fileName) {
  string fstr;
  string_init(&fstr);

  FILE* fd = fopen(fileName, "r");
  fseek(fd, 0L, SEEK_END);
  
  size_t sz = ftell(fd);
  string_setLength(&fstr, sz);
  
  rewind(fd);
  fread(fstr.ptr, sz, 1, fd);
  fclose(fd);
  
  return fstr;
}

int askForInteger() {
  printw("> ");
  refresh();

  echo();
  curs_set(1);
  int input;
  scanw("%d", &input);
  noecho();
  curs_set(0);

  return input;
}

string askForString() {
  printw("> ");
  refresh();

  echo();
  curs_set(1);
  string fstr = string_empty();
  char test[MAX_INPUT_LENGTH];
  getstr(test);
  noecho();
  curs_set(0);

  string_append(&fstr, test);

  return fstr;
}

void printProgramHeader() {
  attron(COLOR_PAIR(1));
  printw("*    ____ ____  _                       _   *\n");
  printw("*   / ___|  _ \\(_)___  ___ ___  _ __ __| |  *\n");
  printw("*  | |   | | | | / __|/ __/ _ \\| '__/ _` |  *\n");
  printw("*  | |___| |_| | \\__ \\ (_| (_) | | | (_| |  *\n");
  printw("*   \\____|____/|_|___/\\___\\___/|_|  \\__,_|  *\n");
  printw("* The easiest tool to send Discord messages *\n");
  printw("*        through a terminal emulator        *\n\n");
  attroff(COLOR_PAIR(1));
  refresh();
}

int menu(stringList options, char* title, int height) {
  size_t length = stringList_length(options);
  ITEM** items = (ITEM**) calloc((int) length + 1, sizeof(ITEM*));
  for (int i = 0; i < (int) length; i++)
    items[i] = new_item(stringList_at(options, i).ptr, "");

  move(height, 0);
  clrtoeol();
  printw("%s\n", title);

  MENU* menu = new_menu(items);
  menu_opts_on(menu, O_ONEVALUE);
  set_menu_back(menu, COLOR_PAIR(2));
  set_menu_sub(menu, derwin(stdscr, 0, 0, height + 1, 0));
  set_menu_mark(menu, "");
  post_menu(menu);
  refresh();

  int choice = -1;
  while(choice == -1) {
    switch(getch()) {
      case KEY_DOWN:
        menu_driver(menu, REQ_DOWN_ITEM);
        break;
      case KEY_UP:
        menu_driver(menu, REQ_UP_ITEM);
        break;
      case KEY_NPAGE:
        menu_driver(menu, REQ_SCR_DPAGE);
        break;
      case KEY_PPAGE:
        menu_driver(menu, REQ_SCR_UPAGE);
        break;
      case 10: // Enter
        choice = current_item(menu)->index;
        break;
      default:
        break;
    }
    refresh();
  }

  unpost_menu(menu);
  free_menu(menu);
  for (int i = 0; i < (int) length; i++)
    free_item(items[i]);
  free(items);

  return choice;
}

