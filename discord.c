#include <curl/curl.h>
#include <stdlib.h>
#include "discord.h"
#include "globals.h"

size_t appendChunk(char* ptr, size_t size, size_t nmemb, string* s) {
  string_appendWithLength(s, ptr, size * nmemb);
  return size * nmemb;
}

string APIRequestGET(string URI, int verbose) {
  // printf("%s\n", URI.ptr);
  CURL* curl = curl_easy_init();
  CURLcode res;

  if (!curl) {
    fprintf(stderr, "curl init error\n");
    exit(EXIT_FAILURE);
  }

  string responseString = string_empty();

  // Headers
  char* userToken = getUserToken();
  struct curl_slist* headers = NULL;
  headers = curl_slist_append(headers, "User-Agent: CDiscord(https://lefra.net, v0.0.1)");
  string authHeaderString = string_empty();
  string_append(&authHeaderString, "Authorization: ");
  if (getUserIsBot()) string_append(&authHeaderString, "Bot ");
  string_append(&authHeaderString, userToken);
  headers = curl_slist_append(headers, authHeaderString.ptr);

  // Options and blocking perform
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_URL, URI.ptr);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseString);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, appendChunk);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, verbose);
  res = curl_easy_perform(curl);

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    exit(EXIT_FAILURE);
  }

  curl_easy_cleanup(curl);
  curl_slist_free_all(headers);
  string_destroy(&authHeaderString);

  return responseString;
}

string APIRequestPOST(string URI, string data, int verbose) {
  // printf("%s\n", URI.ptr);
  CURL* curl = curl_easy_init();
  CURLcode res;

  if (!curl) {
    fprintf(stderr, "curl init error\n");
    exit(EXIT_FAILURE);
  }

  string responseString = string_empty();

  // Headers
  char* userToken = getUserToken();
  struct curl_slist* headers = NULL;
  headers = curl_slist_append(headers, "User-Agent: CDiscord(https://lefra.net, v0.0.1)");
  string authHeaderString = string_empty();
  string_append(&authHeaderString, "Authorization: ");
  if (getUserIsBot()) string_append(&authHeaderString, "Bot ");
  string_append(&authHeaderString, userToken);
  headers = curl_slist_append(headers, authHeaderString.ptr);

  // Options and blocking perform
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_URL, URI.ptr);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.ptr);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseString);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, appendChunk);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_VERBOSE, verbose);
  res = curl_easy_perform(curl);

  if(res != CURLE_OK) {
    fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    exit(EXIT_FAILURE);
  }

  curl_easy_cleanup(curl);
  curl_slist_free_all(headers);
  string_destroy(&authHeaderString);

  return responseString;
}

cJSON* getGuilds () {
  string requestURIString = string_empty();
  string_append(&requestURIString, "https://discord.com/api/v9/users/@me/guilds");

  string responseString = APIRequestGET(requestURIString, 0);
  cJSON* parsedResult = cJSON_ParseWithLength(responseString.ptr, responseString.length);

  string_destroy(&requestURIString);
  string_destroy(&responseString);

  return parsedResult;
}

cJSON* getChannels(char* guildId) {
  string requestURIString = string_empty();
  string_append(&requestURIString, "https://discord.com/api/v9/guilds/");
  string_append(&requestURIString, guildId);
  string_append(&requestURIString, "/channels");

  string responseString = APIRequestGET(requestURIString, 0);
  cJSON* parsedResult = cJSON_ParseWithLength(responseString.ptr, responseString.length);

  string_destroy(&requestURIString);
  string_destroy(&responseString);

  return parsedResult;
}

cJSON* getChannel(char* channelId) {
  string requestURIString = string_empty();
  string_append(&requestURIString, "https://discord.com/api/v9/channels/");
  string_append(&requestURIString, channelId);

  string responseString = APIRequestGET(requestURIString, 0);
  cJSON* parsedResult = cJSON_ParseWithLength(responseString.ptr, responseString.length);

  string_destroy(&requestURIString);
  string_destroy(&responseString);

  return parsedResult;
}

cJSON* getMessages (char* channelId) {
  string requestURIString = string_empty();
  string_append(&requestURIString, "https://discord.com/api/v9/channels/");
  string_append(&requestURIString, channelId);
  string_append(&requestURIString, "/messages");

  string responseString = APIRequestGET(requestURIString, 0);
  cJSON* parsedResult = cJSON_ParseWithLength(responseString.ptr, responseString.length);

  string_destroy(&requestURIString);
  string_destroy(&responseString);

  return parsedResult;
}

cJSON* sendMessage(char* channelId, char* message) {
  string requestURIString = string_empty();
  string_append(&requestURIString, "https://discord.com/api/v9/channels/");
  string_append(&requestURIString, channelId);
  string_append(&requestURIString, "/messages");

  string requestDataString = string_empty();
  string_append(&requestDataString, "content=");
  string_append(&requestDataString, message);

  string responseString = APIRequestPOST(requestURIString, requestDataString, 0);
  cJSON* parsedResult = cJSON_ParseWithLength(responseString.ptr, responseString.length);

  string_destroy(&requestURIString);
  string_destroy(&requestDataString);
  string_destroy(&responseString);

  return parsedResult;
}