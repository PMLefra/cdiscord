//
// Created by PMLefra on 14/06/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include "list.h"

stringList stringList_first(stringList sl) {
  while (sl->prev != NULL)
    sl = sl->prev;

  return sl;
}

stringList stringList_last(stringList sl) {
  while (sl->next != NULL)
    sl = sl->next;

  return sl;
}

void stringList_destroy(stringList sl) {
  while(stringList_length(sl) > 0)
    stringList_pop(sl);

  free(sl);
}

stringList stringList_empty() {
  stringList sl = malloc(sizeof(struct stringList));
  sl->val = NULL;
  sl->next = NULL;
  sl->prev = NULL;
  return sl;
}

void stringList_append(stringList sl, string str) {
  string* strCopy = malloc(sizeof(string));
  string_init(strCopy);
  string_append(strCopy, str.ptr);

  sl = stringList_last(sl);

  if (sl->val == NULL)
    sl->val = strCopy;
  else {
    stringList slNext = malloc(sizeof(struct stringList));
    slNext->val = strCopy;
    slNext->prev = sl;
    slNext->next = NULL;
    sl->next = slNext;
  }
}

void stringList_pop(stringList sl) {
  size_t length = stringList_length(sl);
  if (length == 0) return;

  sl = stringList_last(sl);

  string_destroy(sl->val);
  free(sl->val);
  sl->val = NULL;

  if (sl->prev != NULL)
    sl->prev->next = NULL;

  if (length > 1) free(sl);
}

size_t stringList_length(stringList sl) {
  sl = stringList_first(sl);
  size_t count = 0;

  if (sl->val != NULL) count++;

  while (sl->next != NULL) {
    sl = sl->next;
    count++;
  }

  return count;
}

string stringList_at(stringList sl, int index) {
  sl = stringList_first(sl);
  size_t pos = 0;

  while ((int) pos < index && sl->next != NULL) {
    sl = sl->next;
    pos++;
  }

  return *sl->val;
}

void stringList_print(stringList sl) {
  sl = stringList_first(sl);
  if (sl->val == NULL) return;

  while (sl != NULL) {
    printf("-> %s \n", sl->val->ptr);
    sl = sl->next;
  }
}
